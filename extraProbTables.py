# Table indices: 0 HH, 1 C, 2 TT, 3 R
# 0 nice, 1 selfish, 2 fortunate, 3 unfortunate, 4 silent, 5 concession

import numpy as np
from actionList import actionList


def oppTable(negotiations):
    table = np.zeros((2, 4))

    # generate table according to logs
    for agents in negotiations["logs"]:
        behaviour = actionList(str(agents["fname"]))

        for i in range(1, len(behaviour["agent1"])):
            if behaviour["agent1"][i] == behaviour["agent2"][i]:
                table[1,  agents["strat"][0]] = table[1,  agents["strat"][0]] + 1
            else:
                table[0,  agents["strat"][0]] = table[0,  agents["strat"][0]] + 1
        for i in range(len(behaviour["agent2"])):
            if behaviour["agent2"][i] == behaviour["agent1"][i]:
                table[1,  agents["strat"][1]] = table[1,  agents["strat"][1]] + 1
            else:
                table[0,  agents["strat"][1]] = table[0,  agents["strat"][1]] + 1

    # normalize
    table = table / table.sum(axis=0)

    return table


def ownTable(negotiations):
    table = np.zeros((2, 4))

    # generate table according to logs
    for agents in negotiations["logs"]:
        behaviour = actionList(str(agents["fname"]))

        for i in range(1, len(behaviour["agent1"])):
            if behaviour["agent1"][i] == behaviour["agent1"][i-1]:
                table[1,  agents["strat"][0]] = table[1,  agents["strat"][0]] + 1
            else:
                table[0,  agents["strat"][0]] = table[0,  agents["strat"][0]] + 1
        for i in range(1, len(behaviour["agent2"])):
            if behaviour["agent2"][i] == behaviour["agent2"][i-1]:
                table[1,  agents["strat"][1]] = table[1,  agents["strat"][1]] + 1
            else:
                table[0,  agents["strat"][1]] = table[0,  agents["strat"][1]] + 1

    # normalize
    table = table / table.sum(axis=0)

    return table
