# HMM Strategy

This code, written in python, calculates the probability of four bidding strategies given a sequence of bids from two agents.

## Getting Started
### Prerequisites

The files depend on two external libraries, namely tabulate(pypi.org/project/tabulate/) and numpy(pypi.org/project/numpy/). To install:

```
pip install tabulate
pip install numpy
```

All training data is automatically retrieved from the subdirectory 'training/'. The test data is retrieved from the subdirectory 'test/'. The two subdirectories should be placed in the same directory as main.py. 

## Running the tests

The following code should be used to run the code from console:

```
python main.py test_filename.json
```
where test_filename is the name of the file to be tested.

## Results
The result will be display in the console after running the code. Three tabels will be shown. The first table represents the sensor model without the addition of the addition of the extra observations. The other two tables represent the probability of the four bidding strategies for agent 1 and agent 2 respectively.