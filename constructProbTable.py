# Table indices: 0 HH, 1 C, 2 TT, 3 R
# 0 nice, 1 selfish, 2 fortunate, 3 unfortunate, 4 silent, 5 concession

import numpy as np 
from actionList import actionList

def constructProbTable(negotiations):
        table = np.zeros((6,4))
        
        # generate table according to logs
        for agents in negotiations["logs"]:
            behaviour = actionList(str(agents["fname"]))
            for step in behaviour["agent1"]:
                table[int(step), agents["strat"][0]] = table[int(step), agents["strat"][0]] + 1                
            for step in behaviour["agent2"]:
                table[int(step), agents["strat"][1]] = table[int(step), agents["strat"][1]] + 1
        
        # normalize
        table = table / table.sum(axis=0)
        
        return table
    
