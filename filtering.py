import numpy as np


def filter(priorProb, transitionModel, sensorModel1, sensorModel2, sensorModel3, observed, agent=1):
    prob = priorProb
    agentname = str("agent" + agent)

    # Opponent Model
    if agent == 1:
        steps = np.array(observed["agent1"][1:])
        stepsOpp = np.array(observed["agent2"][:-1])
    else:
        steps = np.array(observed["agent2"][1:])
        stepsOpp = np.array(observed["agent1"][1:])

    opptest = steps == stepsOpp

    # Own Model
    steps1 = np.array(observed[agentname][1:])
    steps2 = np.array(observed[agentname][:-1])
    owntest = steps1 == steps2

    steps = observed[agentname]
    for i in range(len(observed[agentname])):
        step = int(steps[i])

        # Transition step
        newProb = np.zeros((len(prob), 1))
        for j in range(len(priorProb)):
            newProb = newProb+prob[j]*transitionModel[:, [j]]
        prob = newProb

        # Update step
        if i == 0:
            probEvidence = np.multiply(sensorModel1[[step], :].T, prob)
        else:
            k = 1 if opptest[i-1] else 0
            l = 1 if owntest[i-1] else 0
            probEvidenceVector = np.multiply(sensorModel1[[step], :].T, sensorModel2[[k], :].T)
            probEvidenceVector = np.multiply(probEvidenceVector, sensorModel3[[l], :].T)
            probEvidence = np.multiply(probEvidenceVector, prob)
        # Normalize
        prob = probEvidence/np.sum(probEvidence)

    return prob
