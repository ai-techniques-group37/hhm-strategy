# -*- coding: utf-8 -*-
import json
import numpy as np


def actionList(Json):
    fileName = Json

    with open(fileName) as data_file:
        data = json.load(data_file)

    issues = data.get('issues')

    issueList = list(issues.keys())

    bids = data.get('bids')
    Util1 = data.get('Utility1')
    Util2 = data.get('Utility2')

    UtilList11 = np.zeros(len(bids),)
    UtilList12 = np.zeros(len(bids),)
    UtilList21 = np.zeros(len(bids),)
    UtilList22 = np.zeros(len(bids),)

    for bid in bids:
        i = int(bid.get('round'))
        if bid.get('accept') is None:
            bid1 = bid.get('agent1').split(',')
            bid2 = bid.get('agent2').split(',')
            for value in range(len(bid1)):
                UtilList11[i] += Util1.get(issueList[value]).get(
                    'weight') * Util1.get(issueList[value]).get(bid1[value])
                UtilList12[i] += Util2.get(issueList[value]).get(
                    'weight') * Util2.get(issueList[value]).get(bid1[value])
                UtilList22[i] += Util2.get(issueList[value]).get(
                    'weight') * Util2.get(issueList[value]).get(bid2[value])
                UtilList21[i] += Util1.get(issueList[value]).get(
                    'weight') * Util1.get(issueList[value]).get(bid2[value])
        else:
            if bid.get('accept') == 'agent1':
                UtilList11[i] = 1
            if bid.get('accept') == 'agent2':
                UtilList22[i] = 1

    ActList1 = np.zeros(len(bids)-2)
    ActList2 = np.zeros(len(bids)-2)
    silentStep = 0.005

    for i in range(1, len(UtilList11)-1):
        delta11 = UtilList11[i] - UtilList11[i-1]  # dx1
        delta12 = UtilList12[i] - UtilList12[i-1]  # dy1
        delta22 = UtilList22[i] - UtilList22[i-1]  # dy2
        delta21 = UtilList21[i] - UtilList21[i-1]  # dx2

        d = i-1
        # 0 nice, 1 selfish, 2 fortunate, 3 unfortunate, 4 silent, 5 concession
        if ((abs(delta11) <= silentStep) & (abs(delta12) <= silentStep)):
            ActList1[d] = 4
        elif (delta11 > silentStep) & (abs(delta12) <= silentStep):
            ActList1[d] = 0
        elif (delta11 > 0):
            if (delta12 > 0):
                ActList1[d] = 2
            else:
                ActList1[d] = 1
        else:
            if (delta12 > 0):
                ActList1[d] = 5
            else:
                ActList1[d] = 3

        if ((abs(delta21) <= silentStep) & (abs(delta22) <= silentStep)):
            ActList2[d] = 4
        elif ((abs(delta21) <= silentStep) & (delta22 > silentStep)):
            ActList2[d] = 0
        elif (delta21 > 0):
            if (delta22 > 0):
                ActList2[d] = 2
            else:
                ActList2[d] = 5
        else:
            if (delta22 > 0):
                ActList2[d] = 1
            else:
                ActList2[d] = 3

    ListDict = {"agent1": ActList1, "agent2": ActList2}
    return ListDict
