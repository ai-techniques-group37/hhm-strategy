import numpy as np
import sys
import os
from tabulate import tabulate
from actionList import actionList
from constructProbTable import constructProbTable
from extraProbTables import oppTable, ownTable
from filtering import filter

# generate the sensor model
print('Generating sensor model from training logs...')
files = os.listdir('training/')
strategies = ["hardheaded", "conceder", "tft", "random"]
negotiations = []
for f in files:
    agent1 = strategies.index(f.split("_")[0])
    agent2 = strategies.index(
        ''.join([i for i in (f.split("_")[1].split(".")[0]) if not i.isdigit()]))
    negotiations.append(
        dict([("fname", 'training/' + f), ("strat", [agent1, agent2])]))
negotiations = dict([("logs", negotiations)])

sensorModel = constructProbTable(negotiations)
oppModel = oppTable(negotiations) # extra sensor model
ownModel = ownTable(negotiations) # extra sensor model

# print the sensor model
print('Sensor model:')
moves = ['nice', 'selfish', 'fortunate', 'unfortunate', 'silent', 'concession']
tab = []
i = 0
for move in moves:
    tab.append([moves[i]] + sensorModel[i].tolist())
    i += 1

print(tabulate(tab, headers=['E', 'E|HardHeaded', 'E|Concession',
                             'E|Tit-for-Tat', 'E|Random'], tablefmt='orgtbl'))
print('')

# print the sensor model
print('Compared to Opponent Moves:')
moves = ['diff', 'same']
tab = []
i = 0
for move in moves:
    tab.append([moves[i]] + oppModel[i].tolist())
    i += 1

print(tabulate(tab, headers=['E', 'E|HardHeaded', 'E|Concession',
                             'E|Tit-for-Tat', 'E|Random'], tablefmt='orgtbl'))
print('')

# print the sensor model
print('Compared to Own Last Moves:')
moves = ['diff', 'same']
tab = []
i = 0
for move in moves:
    tab.append([moves[i]] + ownModel[i].tolist())
    i += 1

print(tabulate(tab, headers=['E', 'E|HardHeaded', 'E|Concession',
                             'E|Tit-for-Tat', 'E|Random'], tablefmt='orgtbl'))
print('')

# filter with the test data
if (len(sys.argv) == 1):
    testfile = 'conceder_random3.json'
else:
    testfile = sys.argv[1]

print('filtering with testfile ' + testfile)
priorProb = 1/4*np.ones((4, 1))
transitionModel = np.diag(np.ones(4,))
observed = actionList('test/' + testfile)

# Calculate probalility with filtering by calling the function filter
prob1 = filter(priorProb, transitionModel, sensorModel, oppModel, ownModel, observed, "1")
prob2 = filter(priorProb, transitionModel, sensorModel, oppModel, ownModel, observed, "2")

# Print table with probabilities
print('Agent 1 strategy probabilities: ')
print(tabulate([['HardHeaded', prob1[0]], ['Concession', prob1[1]], [
      'Tit-for-Tat', prob1[2]], ['Random', prob1[3]]], headers=['X', 'P(X)'], tablefmt='orgtbl'))
print('')
print('Agent 2 strategy probabilities: ')
print(tabulate([['HardHeaded', prob2[0]], ['Concession', prob2[1]], [
      'Tit-for-Tat', prob2[2]], ['Random', prob2[3]]], headers=['X', 'P(X)'], tablefmt='orgtbl'))
