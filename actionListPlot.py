# -*- coding: utf-8 -*-
import json
import numpy as np
import matplotlib.pyplot as plt

def actionListPlot(Json):
    fileName = 'training/' + Json

    with open(fileName) as data_file:
        data = json.load(data_file)

    issues = data.get('issues')

    issueList = list(issues.keys())

    bids = data.get('bids')
    Util1 = data.get('Utility1')
    Util2 = data.get('Utility2')

    UtilList11 = np.zeros(len(bids),)
    UtilList12 = np.zeros(len(bids),)
    UtilList21 = np.zeros(len(bids),)
    UtilList22 = np.zeros(len(bids),)

    for bid in bids:
        i = int(bid.get('round'))
        if bid.get('accept') is None:
            bid1 = bid.get('agent1').split(',')
            bid2 = bid.get('agent2').split(',')
            for value in range(len(bid1)):
                UtilList11[i] += Util1.get(issueList[value]).get(
                    'weight') * Util1.get(issueList[value]).get(bid1[value])
                UtilList12[i] += Util2.get(issueList[value]).get(
                    'weight') * Util2.get(issueList[value]).get(bid1[value])
                UtilList22[i] += Util2.get(issueList[value]).get(
                    'weight') * Util2.get(issueList[value]).get(bid2[value])
                UtilList21[i] += Util1.get(issueList[value]).get(
                    'weight') * Util1.get(issueList[value]).get(bid2[value])
        else:
            if bid.get('accept') == 'agent1':
                UtilList11[i] = 1
            if bid.get('accept') == 'agent2':
                UtilList22[i] = 1

    ActList1 = np.zeros(len(bids)-2)
    ActList2 = np.zeros(len(bids)-2)
    silentStep = 0.01
    
    plt.figure(figsize=(20,20))
    # silentbox
    plt.plot([silentStep,silentStep],[-silentStep,silentStep],'-k')
    plt.plot([-silentStep,-silentStep],[-silentStep,silentStep],'-k')
    plt.plot([-silentStep,silentStep],[silentStep,silentStep],'-k')
    plt.plot([-silentStep,silentStep],[-silentStep,-silentStep],'-k')
    
    # nicebox
    plt.plot([silentStep,0.1],[silentStep,silentStep],'-k')
    plt.plot([silentStep,0.1],[-silentStep,-silentStep],'-k')
    
    # remaining quarters
    plt.plot([0,0],[-0.1,-silentStep],'-k')
    plt.plot([-0.1,-silentStep],[0,0],'-k')
    plt.plot([0,0],[silentStep,0.1],'-k')
    
    # 0 nice, 1 selfish, 2 fortunate, 3 unfortunate, 4 silent, 5 concession
    plt.text(-0.004,-0.004,'4')
    plt.text(0.054,-0.004,'0')
    plt.text(0.054,0.054,'2')
    plt.text(0.054,-0.054,'1')
    plt.text(-0.054,0.054,'5')
    plt.text(-0.054,-0.054,'3')
 
       
    plt.xlabel('U_own')
    plt.ylabel('U_oponent')
    plt.xlim([-0.1,0.1])
    plt.ylim([-0.1,0.1])
    plt.axis('equal')
   
    
    maxShift = np.zeros(len(UtilList11))
    
    for i in range(1, len(UtilList11)-1):
        delta11 = UtilList11[i] - UtilList11[i-1]  # dx1
        delta12 = UtilList12[i] - UtilList12[i-1]  # dy1
        delta22 = UtilList22[i] - UtilList22[i-1]  # dy2
        delta21 = UtilList21[i] - UtilList21[i-1]  # dx2
        if (abs(delta11) > 0.1):
            dpx = 0.07 * delta11/abs(delta11)
        else:
            dpx = delta11
        if (abs(delta12) > 0.1):
            dpy = 0.07 * delta12/abs(delta12)
        else:
            dpy = delta12
            
        if (abs(delta21) > 0.1):
            dpx2 = 0.07 * delta21/abs(delta21)
        else:
            dpx2 = delta21
        if (abs(delta22) > 0.1):
            dpy2 = 0.07 * delta22/abs(delta22)
        else:
            dpy2 = delta22    
            
        #plt.text(dpx,dpy,str(i),fontsize=8)
        
        maxShift[i] = np.max([delta11, delta12, delta21, delta22])
        d = i-1
        # 0 nice, 1 selfish, 2 fortunate, 3 unfortunate, 4 silent, 5 concession
        if ((abs(delta11) <= silentStep) & (abs(delta12) <= silentStep)):
            ActList1[d] = 4
        elif (delta11 > silentStep) & (abs(delta12) <= silentStep):
            ActList1[d] = 0
        elif (delta11 > 0):
            if (delta12 > 0):
                ActList1[d] = 2
            else:
                ActList1[d] = 1
        else:
            if (delta12 > 0):
                ActList1[d] = 5
            else:
                ActList1[d] = 3

        if ((abs(delta21) <= silentStep) & (abs(delta22) <= silentStep)):
            ActList2[d] = 4
        elif ((abs(delta21) <= silentStep) & (delta22 > silentStep)):
            ActList2[d] = 0
        elif (delta21 > 0):
            if (delta22 > 0):
                ActList2[d] = 2
            else:
                ActList2[d] = 5
        else:
            if (delta22 > 0):
                ActList2[d] = 1
            else:
                ActList2[d] = 3
        plt.text(dpy2,dpx2,str(ActList2[d]),fontsize = 8,color='red')        
        plt.text(dpx,dpy,str(ActList1[d]),fontsize = 8)        
    
    plt.show()
    ListDict = {"agent1": ActList1, "agent2": ActList2}
    print(np.max(maxShift))
    return ListDict
